module.exports = {
  apps: [
    {
      name: "strapi",
      cwd: "D:ProyectosReactEcommercemartfury_strapi_api_1_1",
      script: "./server.js",
      watch: true,
      env: {
        NODE_ENV: "development",
      },
      env_production: {
        NODE_ENV: "production",
      },
    },
  ],
};
