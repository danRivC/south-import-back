module.exports = ({env}) => ({
  defaultConnection: "default",
  connections: {
    default: {
      connector: "bookshelf",
      settings: {
        client: "mysql",
        host: "localhost",
        port: 3306,
        username: "root",
        password: "@gu1mA5h0es.",
        database: "wiseecommerce"
      },
      options: {
        useNullAsDefault: true,
      }
    }
  }
})