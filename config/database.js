module.exports = ({env}) => ({
  defaultConnection: "default",
  connections: {
    default: {
      connector: "bookshelf",
      settings: {
        client: "mysql",
        host: "localhost",
        port: 6033,
        username: "root",
        password: "1234",
        database: "southimportecommerce"
      },
      options: {
        useNullAsDefault: true,
      }
    }
  }
})
