'use strict';

const sanitizeEntity = require("strapi-utils/lib/sanitize-entity");

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

const addCustomFields = (productInfo) => {
    const product = sanitizeEntity(productInfo, {
        model: strapi.models.product,
    });
    let promedio = product.reviews.reduce((p, review) => p + review.valor, 0) / product.reviews.length;
    let cantidad1 = product.reviews.filter(review => review.valor == 1).length;
    let cantidad2 = product.reviews.filter(review => review.valor == 2).length;
    let cantidad3 = product.reviews.filter(review => review.valor == 3).length;
    let cantidad4 = product.reviews.filter(review => review.valor == 4).length;
    let cantidad5 = product.reviews.filter(review => review.valor == 5).length;
    product.resumenOpiniones = {
        promedio,
        cantidad1,
        cantidad2,
        cantidad3,
        cantidad4,
        cantidad5,
    };
    return product;
}

module.exports = {
    //GET /products/{id}
    findOne: async ctx => {
        const { id } = ctx.params;
        let entity = await strapi.services.product.findOne({ id });

        return addCustomFields(entity);
    },

    //GET /products
    find: async ctx => {
        let entities = [];
        if (ctx.query._q) {
            entities = await strapi.services.product.search(ctx.query);
        } else {
            entities = await strapi.services.product.find(ctx.query);
        }

        return entities.map(entity => addCustomFields(entity));
    },
};
