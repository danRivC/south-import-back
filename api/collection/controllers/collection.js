"use strict";

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  categories: async (ctx, next) => {
    const collections = strapi.services.collections.find();
    const categories = strapi.services["product-categories"].find();

    try {
      ctx.body = collections;
    } catch (err) {
      ctx.body = err;
    }
  },
};
