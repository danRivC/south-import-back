'use strict';

/**
 * Lifecycle callbacks for the `post` model.
 */
const slugify = require('slugify');

module.exports = {
  lifecycles: {
    beforeCreate: async (data) => {
      if (data.title) {
        data.slug = slugify(data.title.toLowerCase());
      }
    },
    beforeUpdate: async (params, data) => {
      if (data.title) {
        data.slug = slugify(data.title.toLowerCase());
      }
    },
  },
};
