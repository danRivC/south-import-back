"use strict";

/**
 * A set of functions called "actions" for `links-footer`
 */

module.exports = {
  collections: async (ctx, next) => {
    const collections = await strapi.services.collection.find();
    const categories = await strapi.services["product-categories"].find();
    var collections_with_products = [];
    for (const collection of collections) {
      const new_collection = {
        id: collection.id,
        name: collection.name,
        slug: collection.slug,
        products: [],
        categories: [],
      };
      for (const product of collection.products) {
        await new_collection.products.push(product.id);
      }
      await collections_with_products.push(new_collection);
    }

    for (const category of categories) {
      const category_accepted = {
        id: category.id,
        name: category.name,
        slug: category.slug,
      };
      for (const product_category of category.products) {
        for (const collection of collections_with_products) {
          if (collection.products.includes(product_category.id)) {
            await collection.categories.push(category_accepted);
          }
        }
      }
    }

    try {
      ctx.body = collections_with_products;
    } catch (err) {
      ctx.body = err;
    }
  },
};
