"use strict";
const { sanitizeEntity } = require("strapi-utils");
/**
 * A set of functions called "actions" for `most-sellers`
 */

module.exports = {
  sellers: async (ctx, next) => {
    const actual_date = new Date();
    const date_filter = new Date();
    date_filter.setMonth(actual_date.getMonth() - 1);
    console.log(strapi.services);
    const transactions = await strapi.services.transactions.find();
    const most_seller = transactions.filter(
      (transaction) =>
        transaction.created_at >= date_filter &&
        transaction.created_at <= actual_date
    );
    try {
      ctx.body = most_seller;
    } catch (err) {
      ctx.body = err;
    }
  },
  categories: async (ctx, next) => {
    const actual_date = new Date();
    const date_filter = new Date();
    date_filter.setMonth(actual_date.getMonth() - 1);
    let transactions = await strapi.services.transactions.find();
    transactions = transactions.filter(
      (transaction) =>
        transaction.created_at >= date_filter &&
        transaction.updated_at <= actual_date
    );
    const categories = await strapi.services["product-categories"].find();
    const categories_most_seller = categories.map((categorie) => {
      const result = categorie.products.map((product_categorie) => {
        const have_product = transactions.map((transaction) => {
          const results = transaction.products.map((product_transaction) => {
            if (product_transaction.product.id === product_categorie.id) {
              return true;
            }
          });
          return results.filter((result) => result !== undefined);
        });
        return have_product[0];
      });
      if (result[0]) {
        return categorie;
      }
    });
    try {
      ctx.body = categories_most_seller.filter(
        (category) => category !== undefined
      );
    } catch (error) {
      ctx.body = err;
    }
  },
  collections: async (ctx, next) => {
    const actual_date = new Date();
    const date_filter = new Date();
    date_filter.setMonth(actual_date.getMonth() - 1);
    const transactions = await strapi.services.transactions.find();
    try {
      ctx.body = "hola mundo";
    } catch (error) {
      ctx.body = err;
    }
  },

  product_collection: async (ctx, next) => {
    const actual_date = new Date();
    const date_filter = new Date();
    date_filter.setMonth(actual_date.getMonth() - 1);
    let transactions = await strapi.services.transactions.find();
    transactions = transactions.filter(
      (transaction) =>
        transaction.created_at >= date_filter &&
        transaction.updated_at <= actual_date
    );
    const { id } = ctx.params;
    const collection = await strapi.services.collection.findOne({
      id: id,
    });
    const products = collection.products.map((product) => {
      const have_product = transactions.map((transaction) => {
        const result = transaction.products.map((product_transaction) => {
          if (product_transaction.product.id === product.id) {
            return true;
          }
        });
        return result[0];
      });
      if (have_product[0]) {
        return product;
      }
    });

    try {
      ctx.body = products.filter((product) => product !== undefined);
    } catch (error) {
      ctx.body = error;
    }
  },
  brands: async (ctx, next) => {
    const actual_date = new Date();
    const date_filter = new Date();
    date_filter.setMonth(actual_date.getMonth() - 1);
    const transactions = await strapi.services.transactions.find();
    try {
      ctx.body = "hola mundo";
    } catch (error) {
      ctx.body = err;
    }
  },
};
